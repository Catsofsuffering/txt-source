不在此处的某地，遥远而又尽在咫尺的地方。

成功了！一边这麼说着，【幻想】的女神一边擦着额头的汗。

【幻想】制作的巨大的纸张上，怎麼说呢，画着广阔的迷宫。

这是地下城。没有地下城又怎麼谈得上冒险呢。

糟了，是的。地下城裡面还需要怪物。

说起冒险，最先联想起的肯定是地下城和龙，洞窟还有巨人！

要再有陷阱就更不错。是的，那现在是發生了什麼呢。

【幻想】随意地先放了些哥布林在裡面，哥布林是最基础的啦。

但之後却不在继续。该怎麼办呢。

强大冒险者遇上强大的怪物，弱小的怪物也要有和他们实力对应的怪物。

不这样的话冒险就不会精彩，而且也不会让冒险者开心。

一名男性神【真实】突然说道，这裡应该放点那个东西。

【幻想】有些怀疑地确认了一句“真要这样吗？”

因为【真实】平时就老是做一些过分的事情。

或是告诉委托人一些邪恶的事情，搞的冒险者们被背叛，最後自己不得不出面封口。

又或是只给冒险者们探查10个距离的侦察术，却在第11个距离的的位置放置陷阱。

而【真实】只是从虚空之中取出了一本书。

不停地翻动书页，无数邪恶的怪物和陷阱从书裡跳了出来！

然後他迅速地把怪物和陷阱都塞进了迷宫裡、

“啊！”【幻想】不禁张口，而【真实】高声笑了笑，然後说道。

之後只要随便找个邪教团，托个梦就可以了。

这样真的好吗——【幻想】心裡想着，不过已经太晚了。

骰子已经掷了下去。

“……啊”

“不会吧。”

然後他和她来了。