# CONTENTS

自覚なし聖女の異世界モフモフ道中  
無自覺聖女的摸呼摸呼旅程  

作者： 犬魔人  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%87%AA%E8%A6%9A%E3%81%AA%E3%81%97%E8%81%96%E5%A5%B3%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E3%83%A2%E3%83%95%E3%83%A2%E3%83%95%E9%81%93%E4%B8%AD.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/girl/%E7%84%A1%E8%87%AA%E8%A6%BA%E8%81%96%E5%A5%B3%E7%9A%84%E6%91%B8%E5%91%BC%E6%91%B8%E5%91%BC%E6%97%85%E7%A8%8B.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/girl/out/%E7%84%A1%E8%87%AA%E8%A6%BA%E8%81%96%E5%A5%B3%E7%9A%84%E6%91%B8%E5%91%BC%E6%91%B8%E5%91%BC%E6%97%85%E7%A8%8B.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/girl/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/girl/自覚なし聖女の異世界モフモフ道中/導航目錄.md "導航目錄")




## [null](00000_null)

- [第1話　無自覺聖女、選擇職業](00000_null/00010_%E7%AC%AC1%E8%A9%B1%E3%80%80%E7%84%A1%E8%87%AA%E8%A6%BA%E8%81%96%E5%A5%B3%E3%80%81%E9%81%B8%E6%93%87%E8%81%B7%E6%A5%AD.txt)

