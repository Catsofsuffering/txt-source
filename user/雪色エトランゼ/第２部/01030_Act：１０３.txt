103
今日和昨日的会议一样还是没有进展，下班後。


我被费欧娜公主与玛利亚母亲邀请至王城中只有女性的茶会，多亏於此，回宅邸的时间完全被延误了。


费欧娜公主和玛利亚母亲，同时也是客人，热切地给予我他们结婚的故事，对於她们，我的事情对女性来说是很憧憬的，并被要求同意此婚事。


我想，如果是王族的婚礼应该很厲害吧。


从回家的马车车窗中，我看到的是王都辉煌闪耀的夜景。


时间已经很晚了，可是往来的人数依然很多。因炎热、闷热的夜晚，睡不着的人也很多吧?


我现在，不是在玛利亚母亲和布莱斯特父亲的宅邸中，而是在父亲大人待在王都的时间所征借的房子。


是可怜了玛丽亚母亲，但我不能让独自一人的父亲孤独著。


父亲借的是，以前我第一次来王都时所借的那房子。


我将头靠在窗户玻璃边，呵呵的笑着。


第一次来王都那时，大法官和殿下的谒见等等的事才更紧张呢。


魔法人偶的袭击，对了，当时正值被邀请去玛利亚母亲或布莱斯特父亲的宅邸呢。


然後，和西利斯......。


脸颊忽然变得很热。


啊啊啊啊啊啊!


我用头往窗户撞了一下。


不行，现在，不行。


在现在的状况下，我个人，不能只顾自己的事。


魔兽魔兽魔兽......。


在心中念着不快的咒语。


"「小姐，妳怎麼了?」"


从联繫的小窗，掌鞭的老爷爷打了招呼。


"「不要紧，没问题的。」"


我竭尽全力地微笑。


一个个的叹息，凝视著放在膝盖上紧握的拳头。


对西利斯，真的是太爱撒娇了。


但是西利斯的告白并不只是玩乐和恶作剧。


那一定是，鼓起勇气......。


但我这样的话，他就会一直被置之不理。


玛利亚母亲和费欧娜公主热心的谈了谈回忆。


......但是，结婚礼服吗?


西利斯是很直率的，但没有稍微坏一点的地方，就是很好的傢伙。


剑的手腕也很厲害，我跟他就像望尘莫及一样。


但是，我是我。


我有什麼......。


如果穿了结婚礼服的话，优人会说些什麼吧。


笑吧......。


唯独夏奈与陆......。


哎呀，万一有那样的事情，也只是在网罗的无数未来中的一个，那也一定是在打倒祸端魔兽之後，在人们恢復平稳後的谈话。


但在那段时间，我不是这样看优人的身姿的。


因为，要打倒祸端魔兽的那一瞬间，是我和那傢伙的最後一刻......。


是的，必须做的。


不是那样的，不行的。


为了朋友们也。


"「伽娜蒂大人，已经到了。」"


车夫先生的声音，是我猛然回到了现实。


"「伽娜蒂大人，您回来了。工作辛苦了。」"


马车的门开了，莉莉安娜出现。


"「伽娜蒂大人，眼睛是红色的。」"


我咯哧咯哧的揉著眼睛。


"「呵呵，对不起，我又打瞌睡了。」"


我下了马车，身体伸出了手。


然後，开始往宅院的方向走。莉莉安娜从後面拿着行李过来。


"「父亲大人已经休息了吗?」"


"「不，当时他和西利斯殿下喝酒，直到先前。」"


"「西利斯，他也还是有来。」"


"「幾乎上周每天都有，除了星期四。」"


那傢伙，如果有那样的空閒时间，也在玛利亚母亲的地方露个脸就好了啊。


玛丽亚妈妈都快寂寞到爆出来了。


"「伽娜蒂小姐。您吃饭了吗?」"


"「对不起，在此领受。先去洗澡了。」"


"「知道了。」"


那麼，在那之前，向父亲大人与西利斯打个招呼。


洗澡完出来後，再次往客厅往客厅看过去，已经没有父亲的身影了，西利斯躺在沙發上。


"「西利斯，睡了吗?」"


我往西利斯的脸窥视著。


没有反应。


往西利斯的脸颊munimuni的戳了一下。


真是的，没办法啊。


我让莉莉安娜帮我準备毛毯，悄悄地铺在西利斯身上。


既然暖了，就不会感冒了吧。


再一次往西利斯的脸颊看後，我回到自己的房间。


打开窗户。


与白天不同，凉爽的空气吹进来了。闷热的夜晚，但因夜风穿进来，稍微感到凉快。乘着那样的清风，现在正旺盛的夏天，虫鸣叫的声音响遍。


还稍微湿润的头髮，轻轻地摇曳著。


我找靠窗的椅子坐下，往夜晚的院子呆呆地远望着。


明天也有会议。


商谈今後的方针是很重要的事情，不过，到底现在这样就好了吗?


一定有更多需要预先準备的事吧。


把脸颊放在膝上。


优人也很努力。


我也要更加更加的努力......。


我能做到的事。


我是......。


稍微，迷迷糊糊的。


飒飒摇曳的夜风，心情非常的舒服。


"「你，伽娜蒂。」"


听见了声音。


......啊，是吗。


"「你，伽娜蒂。」"


我慢慢地张开了眼睛。


寂静的房间裡没有人影。


那个，刚才吵死人的虫声也停了。


环顾四周。


於是我，吓的抖动了一下身体。


"「......!」"


突然站了起来，很急忙地躲开，睡意再一瞬间一扫而空。


呼吸变的急躁。


心跳得很快，冰冷的汗水在背部留着。


视线固定在窗幕摇动的阳台上，我在脑海中拼命地劝认了剑的位置。


一副铠甲突然响了。


从阳台拨开窗幕进来，与王立骑士团与黄磷骑士团身穿盔甲的人也不同的身影。


"「梵......梵!」"


我像是挤出来的一样，这样的嘟囔著。


不是黑骑士，而是以人类姿态的梵，瞇著眼睛点了点头。


我往後退了一下。。


"「梵，有什麼事吗?」"


声音在颤抖。


因为在我眼前的是，我等无论如何都敌不过的敌人。


"「伽娜蒂。你现在马上离开这裡最好。」"


我皱著眉头。


"「什麼事呢?」"


"「嗯。不久後魔兽就要溢出了。在这个城市啊。」"


胸膛心痛了一下。


魔兽......。


我拼命的往梵身上瞪。


"「妳也是，曾经为了人们与魔兽战鬥吗?停止如此残忍的事吧!」"


"「请別误会。」"


梵轻薄的笑了一下，看到她耸了耸肩。


"「魔兽溢出的事是我，不，是我们的原因。这个体群以不再受中枢控制了。」"


"「......魔兽到底是什麼?」"


然後，勉强挤出了这句话。


"「没错。」"


梵挽著手臂，我不解除战鬥姿态，一点一滴地移动到剑的方向去。


"「魔兽本来就是在天空漆黑的星海上本跑、以星星为粮食虚幻的兽。」"


这个神话叙述的战鬥啊。


从天上来的祸兽，还是?


这样的话，果然是虚幻的野兽，就是那地下沉睡的尸体，然後祸端魔兽呢。


"「一般人类以魔兽称呼著，正是把那样虚幻的野兽从终端解放的阿。」"


就像对孩子那样温柔的声音诉说着，忽然自嘲的笑了。


"「我那个就算被封印的人柱也策动着野外的终端群，他试着吞噬那颗星球。但是，过了很久。万年後，终端群作为魔兽这种生物，建立了独立的生态，恐怕我们也控制不住了。」"


超出想像的事使我目瞪口呆，但有另我牵掛的事。


梵的第一人称，是仆，还是我呢?


客观地说，我也会说自己像是祸端魔兽。


梵的主体，在哪裡呢?


是梵自己吗?是黑骑士吗?是祸端魔兽吗?


"「为何会把这样的事告诉我呢?」"


对我，梵嫣然微笑着。


那笑容，绝对不是与那不详的黑骑士同个人物。


"「说了吧?妳和安聂菲亚很相似。不想看到被魔兽一同吞噬的身姿。」"


我紧咬著臼齿。


"「梵先生，梵先生。安聂菲亚公主，是只顾著自己逃跑的人吗?」"


"「不......。」"


梵的声音，有一点阴沉。


"「安聂菲亚公主的心情的话，他期盼著什麼妳明白的吧?」"


我不知道安聂菲亚小姐直接说了什麼。也只是从马斯坦博士的报告中听到而已。


但是，如果说梵觉得我和安聂菲亚小姐很相似的话。


一定也是这样认为的吧。


"「梵．布莱夫。妳或许会被祸端魔兽给迷惑住。但是，如果你的心还存在的话，现在马上停下魔兽。用你所拥有的力量，介给人民力量吧。」"


我情不自禁地豪言著。


愤怒的梵像我冲来怎麼办呢，这样的事我没考虑。


不过，安聂菲亚公主也是，梵也是，为了保护生命而战鬥。


不要把那个结果当成悲剧。


只是，我是这样想的。


梵沉默著。


我也沉默著，一动也不动的凝视著梵。


令人耳朵疼的寂静。


星光往室内照射了下来。


不久，滴小的笑声回响著。


梵，肩膀抖著笑了。


"「痴痴地笑着，不，你真的很像她。连我的意见也不理，就与他人走向战场了。」"


"「梵......。」"


"「但是，已经不行了。那是，就这样把这个星球吞噬，最为黑色大海的粮食，大家都被吞噬了。沦为黑色的大地。比如，现在逃避魔兽，马上就会变成那样了。」"


"「梵先生，请住手!」"


我不禁向前走了一步。


"「不行啊，伽娜蒂。我，放弃了呢。不，不敢相信了。那是因为我的缘故，安聂菲亚受伤，那......。那个时候，我放弃了，一切都结束了。所以说，我已经没有残存对抗的精力了。」"


梵回头看着。


那时他浮现的笑容，是一半快哭的脸......。


铁铮铮的凯甲回响著。


"「这样最後能够浮现的我的意识，伽娜蒂。与安聂菲亚相同的眼神的妳相遇。」"


梵慢慢往窗幕对面消失。


"「伽娜蒂。最後能和你说话真是太好了。祈祷不会再会面。」"


就这样，梵的气息突然消失了。


我呆然的凝视著摇晃的窗幕一段时间，下定决心往阳台跑了起来。


屋子的後面，小小的庭园扩大著。


当然不能跟投资的豪宅比较，但人仍然长满苍郁茂盛青绿色的树木。


再那之中，我隐约地看见了。


那个，不陌生的盔甲。


锐利突起、突出漆黑的盔甲。










和梵的邂逅之後，我立刻跑了西利斯的地方。


西利斯看起来心灵舒畅地睡着，用力地摇晃了下。


"「西利斯，请起床吧。」"


不起来。


敲了敲额头。


还是不起来。


踢了下去。


"「很烦阿，什麼，伽娜蒂?怎麼，来这呢?」"


终於睁开眼睛的西利斯看着我笑着。


我打掉伸来的手。


"「西利斯，梵，黑骑士来了!」"


我的话，让他一瞬间僵硬后，西利斯的笑容就消失了。然後，眼睛睁得大大的边揉著头爬了起来。


"「......發生什麼事了?」"


我轻轻地深呼吸平静自我。


"「魔兽来临了，也许，在王都。」"


然後我赶紧整顿打扮，奔跑回王城。和梵的对话，在途中也和西利斯报告了一下。


王立骑士团的我们，汇集值班中的骑士，向周围城市与各地驻紮的部队传令警戒状态。同时，也紧急召集即使不值班的骑士与工作人员。


我在王立骑士团的作战会议室，掌握在王都近郊魔兽群的位置。


在地裂峡谷塔出现了，大陆上的魔兽以塔为目标而进行庞大的移动。其中，掌握的几大集团，在地图上标记着。


西利斯排在我排边。


"「应该说美女伽娜蒂很幸运吧。因为黑骑士特意警告了我。」"


"「西利斯......。」"


恶作剧般笑容的西利斯，我瞪视著他。


梵来了，与其说是我什麼的，那傢伙还对安聂菲亚小姐有剩餘强烈的思念。


"「不，不开玩笑了。不管从哪方向来，都不知道迎击的地方，这是个很严重的问题。」"


西利斯用很认真的表情看了看地图。


确实是如此......。


我悄悄的把手放在胸口。


心扑通扑通的跳。


有多少魔兽转移来这，我不是很清楚，不过那样的话站都是无法避免的。


不管怎样的规模，骑士与士兵都会受伤、倒下。


或许，一般市民也......。


我一定得保护他们。


全力以赴。


各部队的布置、在王都近郊的战鬥大纲、参谋部的意见和呈报给国王陛下的报告书等等，处理杂事凌乱的过程中，不知不觉太阳升起了。


常常得夜晚结束後，从王立骑士团本部从窗子看到的天空，也渐渐变白了。


我稍微休息一下，把咖啡倒入两个杯子中。


一个用嘴喝了一口，另一个，递给了西利斯。我的是纯黑咖啡。西利斯的稍微加了些牛奶，并无加少许糖。


"「对不起。」"


西利斯忽然笑了起来。


随着时间的流逝，些许紧张感渐渐的减弱。


难道说梵的话，是会动摇我们的威胁......。


忽然这麼想到的时候。


"「告辞了!」"


骑士闯进来的叫喊声，在会议室回响著。


心裡动搖着。


我紧紧地往杯子注入力量。


"「王都外缘的侦查队伍报告!确认为东北方向的犬型魔兽!正朝这边来了!」"
