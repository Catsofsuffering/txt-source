周围变得暗了起来，究竟跑了多少房间连我自己也数不过来。

感觉一直在一个地方打转。

接触着合金的地面让我的脚变得冰冷，话说从刚才开始自己就没有穿衣服呢。

可是在这裡根本就找不到一件能蔽体的东西，就连一块麻布也找不到。

踱步到了一个较小的房间内，这裡依然没有找到衣物。

肚子也开始咕咕直叫了……以前的自己有这麼不禁饿吗？
就在打算前往下一个房间的时候，脑内响起了警钟。

好像有什麼动静？

有什麼东西正在向这边靠过来，虽然那并不是脚步声，本能的危险预感让我慌张了起来。

不能被抓到，否则会被再关到那个培养器裡面。

明明还没看到对方的身份，但是不知道为什麼我就是会这样想。

在心臟疯狂跳了数秒，自己依旧處於不知所措的慌乱状态，左顾右盼着最後还是躲在最裡面的一个培养器之後。

如果什麼都不看单单躲藏起来是会让人不安的，想必经歷过类似情况的人都有这种感觉。

因为看不见对方而觉得害怕，对方看不到自己就觉得安心，这是生物的一种本能。

我小心翼翼地探出了头，看着从过道中经过的人。

虽然不知道有着纯白双翼浮在空中的女人还能不能算人了，如果说变身之後的我是恶魔的话……这个女人大概就是天使吧。

她的金髮一丝不乱，即使在半空中快速漂浮着通过也感受不到一丝紊乱。

完全行走在一条直线上。犹如工厂的机器那样笔直地通过。

砰……

一声闷响，脚底滑了一下。我撞上了玻璃罩。

为什麼人总会在关键时刻犯低级错误呢？

其实这大概也是有原因的，紧张的时候注意力被对方完全吸引。结果连平时不必特别在意的事情都无法保持了。

虽然我有着充足的理由，但是仍然改变不了即将遭殃的事实。

那个天使般的少女再次宛如机器那样笔直向我的方向飘了过来。

怎麼办！怎麼办！谁来救救我啊！

在脑内拼命而又大声地呼救着。

但是现实的自己却连脚都软了。

失去了炎魔的力量结果连原本那种体力的优势也失去了。

少爷……少爷……

想到少爷的时候视线一瞬间就被泪水弄得模糊了。

在这个时候不是希望别人，而是希望他在这时候跑过来救自己。

希望他立刻出现在面前把我像被巨龙困住的公主那样一鼓作气抱走。

但是我知道的……

——明明去寻求拥抱时已经被他明确拒绝了。

面对竭力去诱-惑的我，少爷念出了希儿的名字。

尽管如此，但是不知道为什麼第一个想到的还是少爷。

不是艾利欧格大人，不是神父大人也不是莲！

我只想要少爷来救我……

“呜……呜……”

明明是危急万分的时刻，自己却软弱地哭泣了。

天使越来越近，感觉自己就要死在这裡了。

然後会在某个容器裡重生吗？

或者不会……

“希……希尔芙……祝福之风！”

艰难地咏唱了咒文。

我连滚带爬地，赤-裸着身体难看地逃跑着。

天使以及发现我了。她的眼眸瞬间变红，朝着我的方向以极快的速度冲过来。

要被追上了。

“这边来！”

“诶？”

穿过连接房间通道的时候，她突然出现在我的面前，然後一把抓住我的手臂将我拉了过去。

之後通道的大门就被紧紧关上而且天使并没有如我想象中的那样拿出阳离子炮一下子轰开合金的大门。

甚至连敲击的声音也没有，我感觉那个有翅膀的少女的气息就这样远离了。

接下来我将视线转向了那个她……

之所以用“她”来称呼这个女孩，那是因为初次之外我想不到其他词。

银白色的长髮延展到了背部，长着大眼睛的那张人偶般的面容和其他容器的少女不同，正微笑着向我表达着善意，而且她穿着华丽看上去就很複杂的组合洋装。看上去却非常合适。

如果不是这种情况的话我一定会问她关於搭配洋装的事情吧，

“那个长着翅膀的女孩是谁……你又是谁，妾身这是在哪裡，真的是死了吗？你能确定这不是在做梦？知道什麼关於破坏神的消息吗？”

听到了我的话。面前这个容貌和我一模一样如同镜子裡面走出来的她露出了稍微有些困扰的神色。

“多疑让你看上去憔悴，但却增添了一点楚楚可怜的魅力~~看呐，颤抖着的身体的不安。多麼想让这副娇躯被一副健壮的臂膀所拥抱~~”

她用着诗一般滑稽的句子躲过了我的追问。

“请正常点，虽然很感谢你出手相救。但是妾身现在没有开玩笑的心情……”

不知道为什麼，对待这个人我一直保持着非常放鬆的态度。

是因为她有着和我相同的容貌吗？

还是说我们有着什麼更深的联繫？

“抱歉。……不过念诗是妾身的习惯，所以这点还请多包容。对了，先从自我介绍开始吧，妾身叫做古尔薇格……嘛，换个你熟悉的名字的话，叫妾身芙莉亚好了。”

这突然的话让我像是遭到了水刑一般顿时开不了声。

“芙莉亚？！你说的是希尔芙利亚的一世女王……不会吧，只是重名而已吧。”

她静静地摇了摇头，把自己身上的大衣披到了我的身上。

“克莉斯……妾身也是被召唤来到这个世界重生的人。如果猜的没错的话你应该也是从地球世界来的吧，因为阿尔克纳拉和那裡的神有交情。妾身原本来自21世纪，故乡是欧洲……能明白吗？”

她突然神情就由微笑变得严肃起来了。

“等等……你说重生。你也是……所以才和妾身长得一样吗？还是说……”

我的大脑彻底混乱了，不是因为她说的话。

而是由她所说的话所想到的一系列可能性……

她说自己是芙莉亚，但是芙莉亚是一百多年前的人。

不过在这个世纪裡长生或者永生并不是什麼难事。

问题在於她说自己所处的时代是21世纪？

时间对不上了……还是说地球世界和阿尔克纳拉的世界不是在一个时间线上的？

而且她刚才还说这个世界的神和地球世界的神有着交情？

这句话严重摧毁了我的三观。

“看来你不是很能理解呢，总之先留个印象吧……详细的事情之後再说明，不如说自己好好想想也能想得通吧，总之这裡不适合说话，我们换个没有天使巡逻的地方吧。”

真的是天使吗，那个少女！

拉起我的手，近乎是相同的体温和相同肌肤的触感。

看着她我就知道自己的模样。

如同人偶一般美丽而梦幻。幼时的干瘪体型也变得丰满了起来。

非要形容的话，这是如同神之手所创造的完美身体。

尽管对於这种如此量产化的完美我有着深深地抵触。

“不必在意那些哟，那些都是没有灵魂的，是我们的备用品哟。除非那个任性的神明大人打算召唤新的爱人了。”

一边拉着我跑着，她一边告知我这些容器中少女的意义。

我无言地收起视线，尽量不去看她们。

“嗯，这裡就差不多了。”

她左拐右拐推开了一道印着纹章的门。

我们来到了一个与之前都不相同的房间……

这裡很特别，也很普通。

在所有犹如SF电影的太空舱房间中，只有这个房间是毫不加以掩饰的幻想风格。也比其他房间要小上很多。

非要说的话，就像魔女的家一样……

没有合金的门和培养容器。

一张有着顶棚的大床，一个看上去有些年代的原木衣柜，立在柜子旁的立镜……以及一个放了许多杂物的小型桌子。

大型家具基本上只有这幾件……

吸引人视线的是壁炉裡巨大的鍊金釜以及中间空出一大块的地板上的巨大魔法阵。

“到了哟。这裡是妾身爱用的房间之一，如果觉得疲惫的话尽管用那张床休息即可，还请不要嫌弃。原.魔女小姐。”

自称芙莉亚的女孩放开了已经被拉得有点痛的我的手臂，做出了一个欢迎的礼。

这一点就和我想象中的芙莉亚有着很大的不同了。

不过比起这个。她刚才称呼我的那个词要更加令人在意。

“原魔女……”

我跟着她轻轻喃喃着。

的确，我现在已经用不出艾利欧格大人所给的力量了。

“对哦。因为你现在已经不是魔女了，克莉斯。”

“不是魔女……因为已经死了吗，果然这裡是死後世界吗？所以才發生这麼多天马行空的事情……”

“这边能理解你混乱的理由哦……妾身当时也花了很多时间才弄明白一部分事情，听妾身说即可。虽然时间不是很多，但是会尽量让你明白个大概的。”

我老老实实地闭上了嘴坐到了那张床上。

虽然不穿衣服坐在别人的床上是极其无礼，甚至让我燃起了强烈的羞耻意识。

於是我打算说最後一句……

“那个……能先给妾身找一件体面的衣服吗？”

我挠了一下自己的左脸颊困扰地说道。(未完待续……)