但是希儿现在又会在哪裡？

穿过了紫鸢尾之舍……在圣息之日的学院内并没有太多学生。

大家趁着好不容易的休息日都应该去逛街了吧。

那麼现在谁最有可能知道希儿的在哪呢……

带着这样的想法我直冲冲地来到校长室前。

可是手却放在门前却迟迟无法无法扣下……

究竟在做什麼啊！我！！

明明都说了要靠自己去了解她，可是现在却连希儿在哪这种事都想不出来。

自己真的是一点都不了解自己的恋人呢。

她的生日是……

完全没去问过。

皇帝陛下知道我们交往的事情吗？

也没关心过。

希儿的爱好是……

做饭或者家务吗？

怎麼可能啊！！那不过是为了让我开心……是她的温柔罢了。

我天天就只想着练剑或者魔法让自己变强，但是实际上有着即使再强大也有守护不了的东西。

喀拉……

一阵低沉的嘎吱声，颇有重量的木门从内侧被推开了。

“怎麼了？一脸忧郁的表情……又遇到麻烦事了？”

学院长挂着一如既往让人看不透的表情走了出来。

她果然在这裡啊，好像她就没有不在这个办公室的时候呢。

不过还是稍微有点烦躁吗？

她交叉着手臂，仿佛等待我先开口一样。

“……学院长打算出去吗？”

“嗯？说什麼呢，我是感觉有人在门口站着半天不打算敲门才过来看看的哦。”

果然是发现我了啊。

不愧是精灵，感知上非常敏锐……我明明既没有發出脚步声也没显露出多余的魔力。

“这样啊……其实也没什麼，那麼打扰了。”

“……等一下。”

“欸？还有什麼事吗，学院长。”

“这是我的台词吧，走到这裡站了半天却说什麼也没有。你在耍我吗，艾文，不会又是希儿的事情吧？”

唔，该怎麼说呢……这也是精灵的力量吗？

督了一眼我的脸，她按着额头，露芙特无奈地叹了一口气。

“别露出那副白痴脸啊，看看你这反应大概就知道是怎麼回事了……又吵架了吗？”

“抱歉，虽然并不是吵架来着……”

我的表情有那麼明显吗？谁看到都知道發生了什麼事吗……

“真是奇怪呢，那孩子平时天天和你黏在一起，实际上关係并不是很好？”

“唔……都是我的错，从来没试着去了解希儿……”

“是吗？但是那孩子却是尽是说关於你的事喔。”

“欸？和学院长吗？”

“当然的吧，虽然那孩子貌似对我没什麼好感，但是她也没什麼朋友，还是会偶尔来这裡喝下午茶的。这也是让你们住在一起的条件之一呢……否则怎麼可能让正值青春期的少年少女啊……”

“说的是呢，不过希儿她并没有做过轻浮的事情呢。”

“当然，是不是，身为精灵的我一眼就能看出来……”

“……”

那是什麼能力啊。

很想这样问，但是现在有更重要的事情要问……

“很可惜今天殿下她并没有来这裡……别那样看着我，艾文先生你应该很忙吧。”

还未等我提问，空气精灵就已经作出回答了。

那麼就没有疑问了，我深深地向学院长鞠了一躬……

“打扰了……”

现在的自己就是慌乱到这种地步吗……

把心情写在了脸上，谁都能一眼看出来。

“……快去做该做的事情吧。皇帝陛下要是知道你们交往女兒还被欺负的事情估计会气得要砍你的头哦。”

“……那还真是，伤脑筋啊。”

皇帝陛下还不知道这件事。本以为学院裡早传的沸沸扬扬了，看来并没有传出太多奇怪的谣言，这也是这个人做的吗？

但是我就连这样的事情也没有关注过……

“嘛，就是这样，别再给我添麻烦了哦，艾文先生。”

“抱歉……”

转过身挥了一下手表示道别，露芙特随後带上办公室的门。

我站直了刚才为了道歉而行礼的身体。

意外地受到了照顾啊，单单是和希儿交往就已经让很多人困扰了啊……

毕竟她是帝国的公主呢。

现在希儿会在哪裡啊……

我幾乎找遍了整个学院，最後来到了赤海葵之厅……嘛，果然不可能在这裡吧。

难得是休息日，也没有什麼学生，毕竟饭菜是出了名的难吃。

“艾文君？”

清澈的声音在我转身打算离开时传了过来。

“中午好，米撒拉小姐。”

“啊，抱歉~~贵安，艾文。”

拿着扫帚的她好像对我的出现非常意外，顿了一下才转为女仆模式捏着裙子向这边行了一礼。

女仆装啊……

虽然是天天看到，但是和我家裡的女仆装的风格完全不一样呢。

莎法尔家的女仆装都是在膝上15公分左右的短裙，荷叶边以及装饰也是非常多。

虽然有点不适合幹活，但是在观赏的角度来说非常有价值……

克莉斯也很适合那身衣服，就像洋娃娃一样可爱。

那好像是爷爷设计的服装……父亲也并不反对所以一直保留了下来。

果然一般的女仆装还是这种样子吧。

“怎麼了，艾文少爷？一直盯着人家看……啊，难道迷上人家了吗？那可不行呢，人家还不想被公主殿下变成虫子哦。嘛，如果是猫的话还可以接受呢~~”

虽然说得像很害怕一样，但是却像说笑话那样轻鬆……

米撒拉小姐好像做什麼都是一副从容的样子。

不过希儿现在失去了创世之钥，已经不能再用魔法了。

但是她好像嘴巴不太牢，为了避免传出去，还是不说为好……

“我也没有想让莱纳森讨厌的意思啊……”

“……莱纳森君？嘛……人家对他并没有那个想法喔~~”

呃，这实在是太残酷了。抱歉啊莱纳森君，她在我面前把你给甩了。

暂时不要告诉他好了……

“总之这边也有事情，还是下次再聊吧，我就先……唔……”

我话音未落，米撒拉小姐突然挽住了我的胳膊。

“等一下啦~~这边也很闲，不如一起去喝杯茶如何~~”

明明刚才还在说怕被希儿施加变形魔法的……所以我完全不知道女孩子究竟在想些什麼啊。

“抱歉……这边真的很忙啦。”

“是关於希尔维亚的事情对吗~~如果是她的话我看见了哦。”

放下了手上的扫帚，米撒拉微微一笑。

“真的吗！！请告诉我。”

“呀……好，好痛。请别这麼粗鲁~~虽然我并不讨厌强硬的艾文喔。”

眨着一只眼睛發出了娇声~~我赶紧放开了抓着她的手……

“失礼了，不过请务必告诉我……希儿她一个人也很危险，喝茶的话之後请你喝多少也没问题。”

“唔，开玩笑啦……希尔维娅殿下她向着城外森林的方向去了哦，因为看上去一脸闷闷的表情我当时也不敢靠近。不好意思，艾文先生~~”

向着森林……

大概就是向着那次赛跑的道路吧。

赛跑？

啊，我明白了……

原来是这麼回事啊。

我果然是个迟钝的笨蛋……

“非常感谢，米撒拉小姐~~”

“嗯，能快点找到就好了呢……而且稍微让人有点嫉妒……啊。”

“诶？”

“不~~什麼都没有哦。”

“这样啊，那麼先走一步了。”

“下次见哦，艾文~~”

女仆重新拿起了扫帚微笑着向我挥手告别。

她说的那个嫉妒……

不会是听错了吧。

不……就算没听错也不要多想……