
各位读者，好久不见了。我是丸山。

在修改战鬥场面的描述时有个小插曲，那就是在实际演练动作时，挥出的左手不小心撞倒装满咖啡牛奶的杯子。咖啡色液体洒满四周著实让我欲哭无泪，床铺

虽然受害，幸好範围不大，稿子幸免于难也算是不幸中的大幸……有兴趣的读者可以找一下弄倒咖啡牛奶的是在哪个场景。

就是那人觉得有乳臭味的地方。

经过如此波折的《ＯＶＥＲＬＯＲＤ2黑暗战士》。如能让各位读者乐在其中，那将是我的荣幸。

这次的故事应该可以推荐给那些已经厌倦老是前往解救女性角色的老梗剧情的读者吧？既然男女平等，那麼前往解救男性的主角也不错吧？虽然主角凡事都会立刻想到自己的利益，如果各位能够喜欢这种颇富心机的角色，我将感到非常高兴。

那麼接下来请让我發表心中的感谢。

这次也替本书画出美丽插画的ＳO－ｂｉｎ大人。成品比作者脑中想像的画面更加精衫，受到完成的插画刺激，让我认真重写了战鬥场景。

再次帮忙完成精美书衣与书腰的Chord Design Studioo帮忙修改、校对难以阅读的部分的大迫大人，这次也非常感谢。编辑F田大人，很多地方都给您添麻烦了。还有请再多增加一点红色吧！不，我知道没有比较好．．．

还有大学时代的好朋友Honey一这次也多谢你了。

然後最应该感谢的是购买本书的各位读者，还有在网路连载时，赐予感想的网

友们，真的非常感谢。大家的感想总会给我满满的动力。

那麼下一集……应该可以比这一集更轻鬆吧……要再重看吗？其实不太想……

不，为了创作有趣的作品就该做到……喔喔，碎碎念就到此为止，差不多要和大家道别了。

我会继续努力，希望第三集还能有机会和大家见面。

下次再会。
